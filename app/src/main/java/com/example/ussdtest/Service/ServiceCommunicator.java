package com.example.ussdtest.Service;

import android.app.Service;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;

public class   ServiceCommunicator extends Service {
    private SMSBReceiver mSMSreceiver;
    private IntentFilter mIntentFilter;

    @Override
    public void onCreate()
    {

        super.onCreate();

        //SMS event receiver
        mSMSreceiver = new SMSBReceiver();
        mIntentFilter = new IntentFilter();
        mIntentFilter.addAction("android.provider.Telephony.SMS_RECEIVED");
        registerReceiver(mSMSreceiver, mIntentFilter);


    }
//    @Override
//    public int onStartCommand(Intent intent, int flags, int startId) {
//    onTaskRemoved(intent);
//
//
//
//        return START_STICKY;
//    }
    @Override
    public void onDestroy()
    {


        // Unregister the SMS receiver
//        Intent SMSBReceiver = new Intent("SMSBReceiver");
//        sendBroadcast(SMSBReceiver);
    }


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

//    @Override
//    public void onTaskRemoved(Intent rootIntent) {
//        Intent SMSBReceiver = new Intent("SMSBReceiver");
//        SMSBReceiver.setPackage(getPackageName());
//
//startService(SMSBReceiver);
//        super.onTaskRemoved(rootIntent);
//    }
}
