//package com.example.ussdtest;
//
//import android.content.ContentResolver;
//import android.content.Context;
//import android.content.Intent;
//import android.content.pm.ResolveInfo;
//import android.database.Cursor;
//import android.net.Uri;
//import android.view.accessibility.AccessibilityManager;
//
//import java.util.ArrayList;
//import java.util.List;
//
//public class CheckAccessibility {
//
//
//    //----------
//    // actually here not just detecting if:
//    //        Accessibility
//    //        but since that can be for other things also ..
//    //        what specifically wanting to detect is:
//    //        Accessibility - TalkBack
//    //
//    //        because that clearest indicator that user is likely to be blind ..
//
//    public static boolean isAccessibilityEnabled(Context context) {
//
//        AccessibilityManager am = (AccessibilityManager) context.getSystemService(Context.ACCESSIBILITY_SERVICE);
//
//        // this can be true even if Addons Detector is on - so is not an indicator if TalkBack is on ..
//        boolean isAccessibilityEnabled_flag = am.isEnabled();
//        boolean isExploreByTouchEnabled_flag = false;
//
//        // using these methods which suggested on stackoverflow for checking if TalkBack is on ..
//        // this only true if Accessibility TalkBack is on ..
//        isExploreByTouchEnabled_flag = isScreenReaderActive_pre_and_postAPI14(context);
//
//
//        return (isAccessibilityEnabled_flag && isExploreByTouchEnabled_flag);
//
//    }
//    //----------
//    // API 14 has method to detect TalkBack .. but for pre-API 14 will use some methods suggested in stackoverflow
//    // http://stackoverflow.com/questions/11831666/how-to-check-if-talkback-is-active-in-jellybean
//    //    https://code.google.com/p/eyes-free/source/browse/trunk/shell/src/com/google/marvin/shell/HomeLauncher.java?spec=svn623&r=623
//
//    private final static String SCREENREADER_INTENT_ACTION = "android.accessibilityservice.AccessibilityService";
//    private final static String SCREENREADER_INTENT_CATEGORY = "android.accessibilityservice.category.FEEDBACK_SPOKEN";
//
//
//    // http://stackoverflow.com/questions/11831666/how-to-check-if-talkback-is-active-in-jellybean
//    //    this seems to be working on HTC Desire (Android 2.3.3)
//    //        and Samsung Galaxy Note (Android 4.0.3) - API 15
//    //
//    private static boolean isScreenReaderActive_pre_and_postAPI14(Context context) {
//
//        Intent screenReaderIntent = new Intent(SCREENREADER_INTENT_ACTION);
//        screenReaderIntent.addCategory(SCREENREADER_INTENT_CATEGORY);
//
//        List<ResolveInfo> screenReaders = context.getPackageManager().queryIntentServices(screenReaderIntent, 0);
//        ContentResolver cr = context.getContentResolver();
//        Cursor cursor = null;
//        int status = 0;
//
//        // http://developer.android.com/reference/java/util/ArrayList.html
//        List<String> runningServices = new ArrayList<>();
//
//        // http://developer.android.com/reference/android/app/ActivityManager.html
//        android.app.ActivityManager manager = (android.app.ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
//        for (android.app.ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
//            runningServices.add(service.service.getPackageName());
//        }
//
//        for (ResolveInfo screenReader : screenReaders) {
//            cursor = cr.query(Uri.parse("content://" + screenReader.serviceInfo.packageName
//                    + ".providers.StatusProvider"), null, null, null, null);
//
//            if (cursor != null && cursor.moveToFirst()) { //this part works for Android <4.1
//                status = cursor.getInt(0);
//                cursor.close();
//                if (status == 1) {
//                    //screen reader active!
//                    return true;
//                } else {
//                    //screen reader inactive
//                    return false;
//                }
//            } else {  //this part works for Android 4.1+
//                if (runningServices.contains(screenReader.serviceInfo.packageName)) {
//                    //screen reader active!
//                    return true;
//                } else {
//                    //screen reader inactive
//                    return false;
//                }
//            }
//        }
//
//        return false;
//    }
//    //----------
//
//}
//
