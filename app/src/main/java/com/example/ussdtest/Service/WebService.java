package com.example.ussdtest.Service;


import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.client.params.CookiePolicy;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.MarshalBase64;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.io.UnsupportedEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.List;

import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class WebService {

    DefaultHttpClient httpClient;
    HttpContext localContext;
    private String ret;
    HttpResponse response1 = null;
    HttpPost httpPost = null;
    HttpGet httpGet = null;
    String webServiceUrl;
    private String SOAP_ACTION = "http://tempuri.org/GetAllATM";
    private String METHOD_NAME = null;
    private String NAMESPACE = "http://tempuri.org/";
    private String URL = "http://192.168.2.120/rrr/WebService1.asmx";
    private List<Param> parametrAll = null;
    private List<Param> header = null;

    // The serviceName should be the name of the Service you are going to be
    // using.
    public WebService(String URLServer) {

        URL = URLServer;

        SOAP_ACTION = NAMESPACE + METHOD_NAME;

    }

    public WebService(String URLServer, List<Param> Hdr, List<Param> parametr) {

        URL = URLServer;
        header = Hdr;
        parametrAll = parametr;

    }

//    public WebService(String URLServer) {
//
//        webServiceUrl = URLServer;
//
//    }

    // Use this method to do a HttpPost\WebInvoke on a Web Service
//    public String webInvoke(String methodName, Map<String, Object> params) {
//
//        JSONObject jsonObject = new JSONObject();
//
//        for (Map.Entry<String, Object> param : params.entrySet()) {
//            try {
//                jsonObject.put(param.getKey(), param.getValue());
//            } catch (JSONException e) {
//                Log.e("Groshie", "JSONException : " + e);
//            }
//        }
//        return webInvoke(methodName, jsonObject.toString(), "application/json");
//    }

    private String webInvoke(String methodName, String data, String contentType) {
        ret = null;

        httpClient.getParams().setParameter(ClientPNames.COOKIE_POLICY, CookiePolicy.RFC_2109);

        httpPost = new HttpPost(webServiceUrl + methodName);
        response1 = null;

        StringEntity tmp = null;

        // httpPost.setHeader("User-Agent", "SET YOUR USER AGENT STRING HERE");
        httpPost.setHeader("Accept",
                "text/html,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5");

        if (contentType != null) {
            httpPost.setHeader("Content-Type", contentType);
        } else {
            httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded");
        }

        try {
            tmp = new StringEntity(data, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            Log.e("Groshie", "HttpUtils : UnsupportedEncodingException : " + e);
        }

        httpPost.setEntity(tmp);

        Log.d("Groshie", webServiceUrl + "?" + data);

        try {
            response1 = httpClient.execute(httpPost, localContext);

            if (response1 != null) {
                ret = EntityUtils.toString(response1.getEntity());
            }
        } catch (Exception e) {
            Log.e("Groshie", "HttpUtils: " + e);
        }

        return ret;
    }

    // Use this method to do a HttpGet/WebGet on the web service
//    public String webGet() throws NoSuchAlgorithmException, KeyManagementException, KeyStoreException,
//            CertificateException, IOException, UnrecoverableKeyException {
//        String getUrl = URL;
//        httpClient = new DefaultHttpClient();
//        //allowAllSSL();
//
//        KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
//        trustStore.load(null, null);
//
//        SSLSocketFactory sf = new MySSLSocketFactory(trustStore);
//        sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
//
//        HttpParams params = new BasicHttpParams();
//        HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
//        HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);
//
//        SchemeRegistry registry = new SchemeRegistry();
//        registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
//        registry.register(new Scheme("https", sf, 443));
//
//        ClientConnectionManager ccm = new ThreadSafeClientConnManager(params, registry);
//
//        if (parametrAll.size() > 0) {
//            getUrl = getUrl + "?";
//        }
//        for (Param P : parametrAll) {
//
//            if (getUrl.endsWith("?")) {
//                try {
//                    getUrl = getUrl + P.Pname + "=" + URLEncoder.encode(P.Pvalue.toString(), "UTF-8");
//                } catch (UnsupportedEncodingException e) {
//                    // TODO Auto-generated catch block
//                    e.printStackTrace();
//                }
//            } else {
//                try {
//                    getUrl = getUrl + "&" + P.Pname + "=" + URLEncoder.encode(P.Pvalue.toString(), "UTF-8");
//                } catch (UnsupportedEncodingException e) {
//                    // TODO Auto-generated catch block
//                    e.printStackTrace();
//                }
//            }
//
//        }
//
//        httpClient = new DefaultHttpClient(ccm, params);
//
//        httpGet = new HttpGet(getUrl);
//        for (Param P : header) {
//
//            httpGet.addHeader(P.Pname, P.Pvalue.toString());
//        }
//
//        // httpGet.setURI(new URI(getUrl));
//        Log.e("WebGetURL: ", getUrl);
//
//        try {
//            response1 = httpClient.execute(httpGet);
//
//        } catch (Exception e) {
//            Log.e("Groshie:", e.getMessage());
//        }
//
//        // we assume that the response body contains the error message
//        try {
//            ret = EntityUtils.toString(response1.getEntity());
//        } catch (Exception e) {
//            Log.e("Groshie:", e.getMessage());
//        }
//
//        return ret;
//    }
//
//    public static JSONObject Object(Object o) {
//        try {
//            return new JSONObject(new Gson().toJson(o));
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        return null;
//    }

//    public InputStream getHttpStream(String urlString) throws IOException {
//        InputStream in = null;
//        int response = -1;
//
//        java.net.URL url = new URL(urlString);
//        URLConnection conn = url.openConnection();
//
//        if (!(conn instanceof HttpURLConnection))
//            throw new IOException("Not an HTTP connection");
//
//        try {
//            HttpURLConnection httpConn = (HttpURLConnection) conn;
//            httpConn.setAllowUserInteraction(false);
//            httpConn.setInstanceFollowRedirects(true);
//            httpConn.setRequestMethod("GET");
//            httpConn.connect();
//
//            response = httpConn.getResponseCode();
//
//            if (response == HttpURLConnection.HTTP_OK) {
//                in = httpConn.getInputStream();
//            }
//        } catch (Exception e) {
//            throw new IOException("Error connecting");
//        } // end try-catch
//
//        return in;
//    }

//    public void clearCookies() {
//        httpClient.getCookieStore().clear();
//    }

//    public void abort() {
//        try {
//            if (httpClient != null) {
//                System.out.println("Abort.");
//                httpPost.abort();
//            }
//        } catch (Exception e) {
//            System.out.println("Your App Name Here" + e);
//        }
//    }

    public String SoapGet() {
        //allowAllSSL();

        Object result = null;
        try {

            SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
            for (Param p : parametrAll) {
                request.addProperty(p.Pname, p.Pvalue);
            }
            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);

            new MarshalBase64().register(envelope); // serialization
            envelope.implicitTypes = true;
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);
            HttpTransportSE androidHttpTransport = new HttpTransportSE(URL, 90000);
            androidHttpTransport.call(SOAP_ACTION, envelope);
            result = (Object) envelope.getResponse();
            return result.toString();

        } catch (Exception e) {
            e.getMessage();
            return "";
        }

    }

//    public String webPost() {
//        ret = null;
//        ArrayList<NameValuePair> postParameters;
//
//        httpPost = new HttpPost(URL);
//        // httpPost.setHeader( "Content-Type", "application/json" );
//
//        httpClient = new DefaultHttpClient();
//        // httpClient.getParams().setParameter(ClientPNames.COOKIE_POLICY,
//        // CookiePolicy.RFC_2109);
//        postParameters = new ArrayList<NameValuePair>();
//
//        if (parametrAll.size() > 0) {
//
//        }
//        for (Param P : parametrAll) {
//
//            try {
//                postParameters.add(new BasicNameValuePair(P.Pname, URLEncoder.encode(P.Pvalue.toString(), "UTF-8")));
//            } catch (UnsupportedEncodingException e) {
//                // TODO Auto-generated catch block
//                e.printStackTrace();
//            }
//
//        }
//
//        for (Param P : header) {
//
//            httpPost.addHeader(P.Pname, P.Pvalue.toString());
//        }
//
//        try {
//            httpPost.setEntity(new UrlEncodedFormEntity(postParameters, "UTF-8"));
//            // StringEntity se = new StringEntity(JSONRequest,"utf-8");
//        } catch (UnsupportedEncodingException e1) {
//            // TODO Auto-generated catch block
//            e1.printStackTrace();
//        }
//
//        response1 = null;
//        Log.d("Groshie", URL + "?" + "");
//
//        try {
//
//            response1 = httpClient.execute(httpPost, localContext);
//
//            if (response1 != null) {
//                ret = EntityUtils.toString(response1.getEntity());
//            }
//        } catch (Exception e) {
//            Log.e("Groshie", "HttpUtils: " + e);
//        }
//
//        return ret;
//    }

    private static TrustManager[] trustManagers;

    public static class _FakeX509TrustManager implements X509TrustManager {
        private static final X509Certificate[] _AcceptedIssuers = new X509Certificate[] {};

        public void checkClientTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
        }

        public void checkServerTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
        }

        public boolean isClientTrusted(X509Certificate[] chain) {
            return (true);
        }

        public boolean isServerTrusted(X509Certificate[] chain) {
            return (true);
        }

        public X509Certificate[] getAcceptedIssuers() {
            return (_AcceptedIssuers);
        }
    }

//    @SuppressLint("TrulyRandom")
//    public static void allowAllSSL() {
//
//        javax.net.ssl.HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
//
//            @Override
//            public boolean verify(String hostname, SSLSession session) {
//                // TODO Auto-generated method stub
//                return true;
//            }
//        });
//
//        SSLContext context = null;
//
//        if (trustManagers == null) {
//            trustManagers = new TrustManager[] { new _FakeX509TrustManager() };
//        }
//
//        try {
//            context = SSLContext.getInstance("TLS");
//            context.init(null, trustManagers, new SecureRandom());
//        } catch (NoSuchAlgorithmException e) {
//            Log.e("allowAllSSL", e.toString());
//        } catch (KeyManagementException e) {
//            Log.e("allowAllSSL", e.toString());
//        }
//        javax.net.ssl.HttpsURLConnection.setDefaultSSLSocketFactory(context.getSocketFactory());
//    }

//    public HttpClient getNewHttpClient() {
//        try {
//            KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
//            trustStore.load(null, null);
//
//            SSLSocketFactory sf = new MySSLSocketFactory(trustStore);
//            sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
//
//            HttpParams params = new BasicHttpParams();
//            HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
//            HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);
//
//            SchemeRegistry registry = new SchemeRegistry();
//            registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
//            registry.register(new Scheme("https", sf, 443));
//
//            ClientConnectionManager ccm = new ThreadSafeClientConnManager(params, registry);
//
//            return new DefaultHttpClient(ccm, params);
//        } catch (Exception e) {
//            return new DefaultHttpClient();
//        }
//    }

//    public String webPostJsonParameters(String Params) {
//        ret = null;
//        // /Jawwal Raw Parameter
//        // httpPost.setEntity(new StringEntity("{\"filters\":true}"));
//        // /
//        httpPost = new HttpPost(URL);
//        httpClient = new DefaultHttpClient();
//
//        try {
//            // httpPost.setEntity(new StringEntity(Params));
//            httpPost.setEntity(new ByteArrayEntity(Params.toString().getBytes("UTF8")));
//
//        } catch (UnsupportedEncodingException e2) {
//            // TODO Auto-generated catch block
//            e2.printStackTrace();
//        }
//
//        for (Param P : header) {
//
//            httpPost.addHeader(P.Pname, P.Pvalue.toString());
//
//        }
//
//        response1 = null;
//        Log.d("Groshie", URL + "?" + "");
//
//        try {
//
//            response1 = httpClient.execute(httpPost, localContext);
//
//            if (response1 != null) {
//                ret = EntityUtils.toString(response1.getEntity(), HTTP.UTF_8);
//            }
//        } catch (Exception e) {
//            Log.e("Groshie", "HttpUtils: " + e);
//        }
//
//        return ret;
//    }

//    public class MySSLSocketFactory extends SSLSocketFactory {
//        SSLContext sslContext = SSLContext.getInstance("TLS");
//
//        public MySSLSocketFactory(KeyStore truststore)
//                throws NoSuchAlgorithmException, KeyManagementException, KeyStoreException, UnrecoverableKeyException {
//            super(truststore);
//
//            TrustManager tm = new X509TrustManager() {
//                public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
//                }
//
//                public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
//                }
//
//                public X509Certificate[] getAcceptedIssuers() {
//                    return null;
//                }
//            };
//
//            sslContext.init(null, new TrustManager[] { tm }, null);
//        }
//
//        @Override
//        public Socket createSocket(Socket socket, String host, int port, boolean autoClose)
//                throws IOException, UnknownHostException {
//            return sslContext.getSocketFactory().createSocket(socket, host, port, autoClose);
//        }
//
//        @Override
//        public Socket createSocket() throws IOException {
//            return sslContext.getSocketFactory().createSocket();
//        }
//    }

}
