//package com.example.ussdtest;
//
//import android.accessibilityservice.AccessibilityService;
//import android.accessibilityservice.AccessibilityServiceInfo;
//import android.content.Intent;
//import android.os.Build;
//import android.support.annotation.RequiresApi;
//import android.text.TextUtils;
//import android.util.Log;
//import android.view.accessibility.AccessibilityEvent;
//import android.view.accessibility.AccessibilityNodeInfo;
//import android.widget.Toast;
//
//import java.io.UnsupportedEncodingException;
//import java.util.Collections;
//import java.util.List;
//
//import retrofit2.Call;
//import retrofit2.Callback;
//import retrofit2.Response;
//
//public class USSDServiceE extends AccessibilityService {
//    public static String TAG = USSDServiceE.class.getSimpleName();
//    String s;
//
//    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
//    @Override
//    public void onAccessibilityEvent(AccessibilityEvent event) {
//        Log.d(TAG, "onAccessibilityEvent");
//
//        AccessibilityNodeInfo source = event.getSource();
//        /* if (event.getEventType() == AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED && !event.getClassName().equals("android.app.AlertDialog")) { // android.app.AlertDialog is the standard but not for all phones  */
//        if (event.getEventType() == AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED && !String.valueOf(event.getClassName()).contains("AlertDialog")) {
//            return;
//        }
//        if(event.getEventType() == AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED && (source == null || !source.getClassName().equals("android.widget.TextView"))) {
//            return;
//        }
//        if(event.getEventType() == AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED && TextUtils.isEmpty(source.getText())) {
//            return;
//        }
//
//        List<CharSequence> eventText;
//
//        if(event.getEventType() == AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED) {
//            eventText = event.getText();
//        } else {
//            eventText = Collections.singletonList(source.getText());
//        }
//        String x = null;
//        for (int i=0;i<=eventText.size();i++){
//            x = eventText.toString();
//        }
//        String text = processUSSDText(eventText);
//
//
//        try
//        {
//            s = new String(text.getBytes(), "UTF-8");
//            Log.e("serwer", "in  try :" + s);
//        }
//        catch (UnsupportedEncodingException e)
//        {
//            Log.e("utf8", "conversion", e);
//        }
//        Log.e(TAG, "onAccessibilityEvent: "+text );
//        Log.e("serwer2222", "out  try :" + s);
//
//
//
//
//
//
//
//
//        if( TextUtils.isEmpty(text) ) return;
//
//        // Close dialog
//        performGlobalAction(GLOBAL_ACTION_BACK); // This works on 4.1+ only
//
//        Log.e("moh", text);
//        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
//        // Handle USSD response here
//        GetUssdResponse service = RetrofitClient.getClient("https://easygate.app/api/").create(GetUssdResponse.class);
//
//        /** Call the method with parameter in the interface to get the notice data*/
//
//        Call<PostResponse> call = service.createUser2("mohamed");
//
//        /**Log the URL called*/
//        Log.e("URL Called", call.request().url() + "");
//
//        call.enqueue(new Callback<PostResponse>() {
//            @Override
//            public void onResponse(Call<PostResponse> call, Response<PostResponse> response) {
//                Toast.makeText(getApplicationContext(), "done " , Toast.LENGTH_SHORT).show();
//                Log.e("sssss",  response.toString() );
//            }
//
//            @Override
//            public void onFailure(Call<PostResponse> call, Throwable t) {
////                Toast.makeText(MainActivity.this,, Toast.LENGTH_SHORT).show();
//                Log.e("sssss",  t.getLocalizedMessage() +t.getStackTrace()+t.getCause() );
//
//            }
//        });
//
//
//    }
//
//    private String processUSSDText(List<CharSequence> eventText) {
//        for (CharSequence s : eventText) {
//            String text = s.toString();
//
//            // Return text if text is the expected ussd response
//            if( true ) {
//                Log.e(TAG, "junaidi: "+s );
//                return text;
//            }
//        }
//
//        return null;
//    }
//
//    @Override
//    public void onInterrupt() {
//    }
//
//    @Override
//    protected void onServiceConnected() {
//        super.onServiceConnected();
//        Toast.makeText(this, "    ", Toast.LENGTH_SHORT).show();
//        Log.d(TAG, "onServiceConnected");
//        AccessibilityServiceInfo info = new AccessibilityServiceInfo();
//        info.flags = AccessibilityServiceInfo.DEFAULT;
//        info.packageNames = new String[]{"com.android.phone"};
//        info.eventTypes = AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED | AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED;
//        info.feedbackType = AccessibilityServiceInfo.FEEDBACK_GENERIC;
//        //CheckAccessibility.isAccessibilityEnabled(this);
//
//        setServiceInfo(info);
//
//    }
//
//    @Override
//    public void onDestroy() {
//        Intent restartService = new Intent("RestartService");
//        sendBroadcast(restartService);
//    }
//}