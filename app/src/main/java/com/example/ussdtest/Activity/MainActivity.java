  package com.example.ussdtest.Activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import com.example.ussdtest.Rertofit.GetNoticeDataService;
import com.example.ussdtest.R;
import com.example.ussdtest.Rertofit.RetrofitClient;
import com.example.ussdtest.Rertofit.post;
import com.google.firebase.iid.FirebaseInstanceId;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

  public class MainActivity extends AppCompatActivity {

EditText etNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);




        startActivityForResult(new Intent(android.provider.Settings.ACTION_ACCESSIBILITY_SETTINGS), 0);



//            GifImageButton gib = new GifImageButton(this);
//            setContentView(gib);
//            gib.setImageResource(R.drawable.visable);
//            final MediaController mc = new MediaController(this);
//            mc.setMediaPlayer((GifDrawable) gib.getDrawable());
//            mc.setAnchorView(gib);
//            gib.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    mc.show();
//                }
//            });

        requestPermissions();
       // Intent myAlarm = new Intent(getApplicationContext(), SMSBReceiver.class);
//myAlarm.putExtra("project_id", project_id); //Put Extra if needed
//        PendingIntent recurringAlarm = PendingIntent.getBroadcast(getApplicationContext(), 0, myAlarm, PendingIntent.FLAG_CANCEL_CURRENT);
//        AlarmManager alarms = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
//        Calendar updateTime = Calendar.getInstance();
//updateTime.setWhatever(0);    //set time to start first occurence of alarm
       // alarms.setInexactRepeating(AlarmManager.RTC_WAKEUP, updateTime.getTimeInMillis(), AlarmManager.INTERVAL_DAY, recurringAlarm);
//        btnCall.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent i = new Intent(Intent.ACTION_CALL);
//                String encodedHash = Uri.encode("#");
//                i.setData(Uri.parse("tel:"+etNumber.getText().toString()+encodedHash));
//                startService(new Intent(MainActivity.this, USSDServiceE.class));
//
//                if (ActivityCompat.checkSelfPermission(MainActivity.this,
//                        Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
//                    return;
//                }
//                startActivity(i);
//
//
//            }
//        });
//        FirebaseMessaging.getInstance().setAutoInitEnabled(true);
//
//        FirebaseInstanceId.getInstance().getInstanceId()
//                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
//                    @Override
//                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
//                        if (!task.isSuccessful()) {
//                            Log.w("hhjhjfhgf44", "getInstanceId failed", task.getException());
//                            return;
//                        }
//
//                        // Get new Instance ID token
//                        String token = task.getResult().getToken();
//
//                        // Log and toast
//                        Log.d("hhjhjfhgf33", token);
//                        Toast.makeText(MainActivity.this, token, Toast.LENGTH_SHORT).show();
//                    }
//                });

        String refreshToken = FirebaseInstanceId.getInstance().getToken();
        Log.e("token", "token "+refreshToken);
        SharedPreferences sharedPreferences = getSharedPreferences("ussd_info", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

//        editor.putString("token",refreshToken);
//        editor.commit();


        if (sharedPreferences.getString("token","") == null){
            editor.putString("token",refreshToken);
            editor.commit();
        } else {

        }
        Intent intent = getIntent();
        String id = intent.getStringExtra("id");
        GetNoticeDataService service = RetrofitClient.getClient("https://easygate.app/api/").create(GetNoticeDataService.class);

        /** Call the method with parameter in the interface to get the notice data*/
        SharedPreferences sp = getSharedPreferences("ussd_info", 0);
        String value = sp.getString("token", null);
        Call<post> call = service.createUser(id,value);

        /**Log the URL called*/
        Log.e("URL Called", call.request().url() + "");

        call.enqueue(new Callback<post>() {
            @Override
            public void onResponse(Call<post> call, Response<post> response) {
                Toast.makeText(MainActivity.this, "done " , Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<post> call, Throwable t) {
//                Toast.makeText(MainActivity.this,, Toast.LENGTH_SHORT).show();
                Log.e("sagtegrfte",  t.getLocalizedMessage() +t.getStackTrace()+t.getCause() );

            }
        });



    }
      public  void   requestPermissions()
      {
          if (ContextCompat.checkSelfPermission(this,
                  Manifest.permission.READ_CONTACTS)
                  != PackageManager.PERMISSION_GRANTED) {

              // Permission is not granted
              // Should we show an explanation?
              if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                      Manifest.permission.CALL_PHONE)) {
                  // Show an explanation to the user *asynchronously* -- don't block
                  // this thread waiting for the user's response! After the user
                  // sees the explanation, try again to request the permission.
              } else {
                  // No explanation needed; request the permission
                  ActivityCompat.requestPermissions(this,
                          new String[]{Manifest.permission.CALL_PHONE},
                          004);

                  // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                  // app-defined int constant. The callback method gets the
                  // result of the request.
              }
          } else {
              // Permission has already been granted
          }
      }
      @Override
      public void onRequestPermissionsResult(int requestCode,
                                             String permissions[], int[] grantResults) {
          switch (requestCode) {
              case 004: {
                  // If request is cancelled, the result arrays are empty.
                  if (grantResults.length > 0
                          && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                      // permission was granted, yay! Do the
                      // contacts-related task you need to do.
                  } else {
                      // permission denied, boo! Disable the
                      // functionality that depends on this permission.
                  }
                  return;
              }

              // other 'case' lines to check for other
              // permissions this app might request.
          }
      }


  }

