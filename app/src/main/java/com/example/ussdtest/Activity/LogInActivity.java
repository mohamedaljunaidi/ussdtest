package com.example.ussdtest.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.ussdtest.Rertofit.GetUser;
import com.example.ussdtest.Rertofit.PostResponse;
import com.example.ussdtest.R;
import com.example.ussdtest.Rertofit.RetrofitClient;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LogInActivity extends AppCompatActivity {
    private static final int REQUEST_CODE_SET_DEFAULT_DIALER = 123;
    TextInputEditText userName;
    TextInputEditText userPassword;
    Button logIn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);
        userName=findViewById(R.id.et_username);
        userPassword=findViewById(R.id.et_password);
//        Intent intent = new Intent(TelecomManager.ACTION_CHANGE_DEFAULT_DIALER)
//                .putExtra(TelecomManager.ACTION_INCOMING_CALL, getPackageName());
//        if (intent.resolveActivity(getPackageManager()) != null) {
//            startActivityForResult(intent, REQUEST_CODE_SET_DEFAULT_DIALER);
//        } else {
//            Log.w(getLocalClassName(), "No Intent available to handle action");
//        }
        SharedPreferences sp = getSharedPreferences("login", 0);
        String name = sp.getString("username", null);
        String pass = sp.getString("password", null);
        if (name != null){
            Intent i = new Intent(this, MainActivity.class);
            Log.e("ssssssss", "onCcccccccccccc: "+ name);
            startActivity(i);
        }


        logIn=findViewById(R.id.btnActivityLoginLogin);
        logIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String username = userName.getText().toString();
                final String userpass = userPassword.getText().toString();

//                if (userName.getText().length() > 0 && userPassword.getText().length() > 0) {
//                    Toasty.success(getApplicationContext(),"login",Toasty.LENGTH_SHORT,true).show();
//              Intent intent = new Intent(getApplicationContext(), MainActivity.class);
//              startActivity(intent);
//                } else {
//                    Toasty.error(getApplicationContext(), "Username or Password is Empty", Toast.LENGTH_SHORT, true).show();
//                }

                GetUser service = RetrofitClient.getClient("https://easygate.app/api/").create(GetUser.class);

                /** Call the method with parameter in the interface to get the notice data*/


                    Call<PostResponse> call = service.createUser2(username,userpass);

                    /**Log the URL called*/
                Log.e("URL Called", call.request().url() + "");

                call.enqueue(new Callback<PostResponse>() {
                        @Override
                    public void onResponse(Call<PostResponse> call, Response<PostResponse> response) {
                        Toasty.success(getApplicationContext(), "login", Toasty.LENGTH_SHORT, true).show();
                        Log.e("mohamedddd", "onResponse: "+response.toString() );
                        String name =response.body().username;
                        String password =response.body().userpassword;
                        SharedPreferences sharedPreferences = getSharedPreferences("login", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("username",name);
                        editor.putString("password",password);
                        editor.commit();



                      String id = response.body().id;
//                      Toast.makeText(LogInActivity.this, id, Toast.LENGTH_SHORT).show();
                      Log.e("asdsccccccccccccc", "onResponse: " + id);

                      Intent i = new Intent(getApplicationContext(), MainActivity.class);
                      i.putExtra("id", response.body().id);

                      startActivity(i);


                    }

                    @Override
                    public void onFailure(Call<PostResponse> call, Throwable t) {
//                Toast.makeText(MainActivity.this,, Toast.LENGTH_SHORT).show();
                        Log.e("sagtegrfte",  t.getLocalizedMessage() +t.getStackTrace()+t.getCause() );
                        Toasty.error(getApplicationContext(), "UserName or Password Wrong", Toast.LENGTH_SHORT, true).show();
                    }
                });



            }

        });




    }
}
