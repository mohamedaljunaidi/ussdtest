package com.example.ussdtest.Rertofit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PostResponse {
    @SerializedName("id")
    @Expose
    public String id ;


    @SerializedName("username")
    @Expose
    public String username ;
    @SerializedName("userpassword")
    @Expose
    public String userpassword ;

    public PostResponse( String username, String userpassword) {

        this.username = username;
        this.userpassword = userpassword;
    }
}
