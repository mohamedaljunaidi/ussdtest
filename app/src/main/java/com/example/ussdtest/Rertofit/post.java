package com.example.ussdtest.Rertofit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class post {

    @SerializedName("id")
    @Expose
    public String id ;

    @SerializedName("name")
    @Expose
    public String name ;

    public post(String id, String name) {
        this.id = id;
        this.name = name;
    }
}
